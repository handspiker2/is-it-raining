<?php
define('WEATHER_CLEAR', 1);
define('WEATHER_RAIN', 2);
define('WEATHER_SNOW', 3);

$status = -1;
$timeStamp = 0;

try {
	$db = new PDO('sqlite:db.sqlite');
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$stmt = $db->prepare('SELECT id, name, rain, snow, update_datetime FROM weather_status WHERE id = 1;');
	$stmt->execute();

	$city = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$stmt->closeCursor();
	$stmt = null;
} catch (PDOException $ex) {
	header('HTTP/1.0 503 Service Unavailable');
	error_log($ex);
	exit();
}

$db = null;

if (count($city) == 1) {
	$name = htmlentities($city[0]['name']);

	if ($city[0]['update_datetime'] != null) {
		$timeStamp = strtotime($city[0]['update_datetime']);
	}

	$city[0]['snow'] = (int) $city[0]['snow'];

	if ((int) $city[0]['rain'] || $city[0]['snow']) {
		$status = WEATHER_RAIN;
		
		if ($city[0]['snow']) {
			$status = WEATHER_SNOW;
		}
	} else {
		$status = WEATHER_CLEAR;
	}
} else {
	header('HTTP/1.0 404 Not Found');
	exit();
}

header('Content-type: application/json');
?>
{"status": <?php echo $status; ?>, "time": <?php echo $timeStamp; ?>}
