<?php
try {
	$db = new PDO('sqlite:' . realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR .'db.sqlite'));
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$stmt = $db->prepare('SELECT id, city_id, rain, snow, update_datetime FROM weather_status;');
	$stmt->execute();

	$cities = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$stmt->closeCursor();
	$stmt = null;
	
	$statuses = array();
	
	$stmt = $db->prepare('SELECT code, name, snow, rain, error FROM yahoo_condition;');
	$stmt->execute();

	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$statuses['c'.$row['code']] = array(
			'name' => $row['name'],
			'snow' => (int) $row['snow'],
			'rain' => (int) $row['rain'],
			'error' => (int) $row['error']
		);
	}

	$stmt->closeCursor();
	$stmt = null;

	foreach ($cities as $city) {
		$query = "select * from weather.forecast where woeid = " . ((int) $city['city_id']) ;
	
		$response = file_get_contents('http://query.yahooapis.com/v1/public/yql?format=json&q=' . urlencode($query));
		$json = json_decode($response, true);

		if ($json !== null && isset($json['query']['results']['channel']['item']['condition']['code'])) {
			$code = 'c' . $json['query']['results']['channel']['item']['condition']['code'];
			
			if (array_key_exists($code, $statuses) && !$statuses[$code]['error']) {
				$sql = '
					UPDATE 
						weather_status
					SET 
						rain = :rain,
						snow = :snow,
						name = :name,
						update_datetime = :time
					WHERE
						id = ' . $city['id']
				;
				
				$stmt = $db->prepare($sql);
				$stmt->execute(array(
					'time' => time(),
					'rain' => $statuses[$code]['rain'],
					'snow' => $statuses[$code]['snow'],
					'name' => isset($json['query']['results']['channel']['location']['city']) ? $json['query']['results']['channel']['location']['city'] : null,
				));
				$stmt->closeCursor();
				$stmt = null;
			}
		}
	}
} catch (Exception $ex) {
	error_log($ex);
}
$db = null;