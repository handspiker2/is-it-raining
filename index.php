<?php
	$city = null;
	$raining = 'No';
	$name = null;
	$time = null;

	try {
		$db = new PDO('sqlite:db.sqlite');
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$stmt = $db->prepare('SELECT id, name, rain, snow, update_datetime, timezone FROM weather_status WHERE id = 1;');
		$stmt->execute();

		$city = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$stmt->closeCursor();
		$stmt = null;
	} catch (Exception $ex) {
		header('HTTP/1.0 503 Service Unavailable');
		error_log($ex);
		exit();
	}
	
	$db = null;
	
	if (count($city) == 1) {
		$name = htmlentities($city[0]['name']);
	
		if ($city[0]['update_datetime'] != null) {
			$time = new DateTime();
			$time->setTimestamp((int)$city[0]['update_datetime']);
			$time->setTimezone(new DateTimeZone(($city[0]['timezone']) ?:'UTC'));
			
			$time = $time->format('M j h:i A T');
		}
	
		$city[0]['snow'] = (int) $city[0]['snow'];

		if ((int) $city[0]['rain'] || $city[0]['snow']) {
			$raining = 'Yes';
			
			if ($city[0]['snow']) {
				$raining  .= " (Snowing)";
			}
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="description" content="Let's you know if it is raining in <?php echo $name; ?>. Plain and simple!" />
	<title>Is It Raining in <?php echo $name; ?>?</title>
	<link href="style.css" type="text/css" rel="stylesheet" />
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-59735545-1', 'auto');
	  ga('send', 'pageview');

	</script>
</head>
<body>
	<h1>Is It Raining in <?php echo $name; ?>?</h1>
	<h2><?php echo $raining; ?></h2>
	<?php if ($time != null) {
		?>
		<span>As of <?php echo htmlentities($time); ?></span>
		<?php
	} ?>
	
	<!--<div>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<ins class="adsbygoogle"
			 style="display:inline-block;width:468px;height:60px"
			 data-ad-client="ca-pub-7569697281526462"
			 data-ad-slot="2332722723"></ins>
		<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
	</div>-->
</body>
</html>