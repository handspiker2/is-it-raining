BEGIN TRANSACTION;
CREATE TABLE "yahoo_condition" (
	`code`	INTEGER NOT NULL UNIQUE,
	`name`	TEXT,
	`rain`	INTEGER NOT NULL DEFAULT 0,
	`snow`	INTEGER NOT NULL DEFAULT 0,
	`error`	INTEGER NOT NULL DEFAULT 0
);

CREATE TABLE "weather_status" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`city_id`	INTEGER NOT NULL,
	`name`	TEXT,
	`rain`	INTEGER NOT NULL DEFAULT 0,
	`snow`	INTEGER NOT NULL DEFAULT 0,
	`update_datetime`	DATETIME
);

INSERT INTO `yahoo_condition` VALUES(0,'tornado',0,0,0);
INSERT INTO `yahoo_condition` VALUES(1,'tropical storm',1,0,0);
INSERT INTO `yahoo_condition` VALUES(2,'hurricane',0,0,0);
INSERT INTO `yahoo_condition` VALUES(3,'severe thunderstorms',1,0,0);
INSERT INTO `yahoo_condition` VALUES(4,'thunderstorms',0,0,0);
INSERT INTO `yahoo_condition` VALUES(5,'mixed rain and snow',1,1,0);
INSERT INTO `yahoo_condition` VALUES(6,'mixed rain and sleet',1,0,0);
INSERT INTO `yahoo_condition` VALUES(7,'mixed snow and sleet',1,1,0);
INSERT INTO `yahoo_condition` VALUES(8,'freezing drizzle',1,0,0);
INSERT INTO `yahoo_condition` VALUES(9,'drizzle',1,0,0);
INSERT INTO `yahoo_condition` VALUES(10,'freezing rain',1,0,0);
INSERT INTO `yahoo_condition` VALUES(11,'showers',1,0,0);
INSERT INTO `yahoo_condition` VALUES(12,'showers',1,0,0);
INSERT INTO `yahoo_condition` VALUES(13,'snow flurries',0,1,0);
INSERT INTO `yahoo_condition` VALUES(14,'light snow showers',0,1,0);
INSERT INTO `yahoo_condition` VALUES(15,'blowing snow',0,1,0);
INSERT INTO `yahoo_condition` VALUES(16,'snow',0,1,0);
INSERT INTO `yahoo_condition` VALUES(17,'hail',0,1,0);
INSERT INTO `yahoo_condition` VALUES(18,'sleet',1,0,0);
INSERT INTO `yahoo_condition` VALUES(19,'dust',0,0,0);
INSERT INTO `yahoo_condition` VALUES(20,'foggy',0,0,0);
INSERT INTO `yahoo_condition` VALUES(21,'haze',0,0,0);
INSERT INTO `yahoo_condition` VALUES(22,'smoky',0,0,0);
INSERT INTO `yahoo_condition` VALUES(23,'blustery',0,0,0);
INSERT INTO `yahoo_condition` VALUES(24,'windy',0,0,0);
INSERT INTO `yahoo_condition` VALUES(25,'cold',0,0,0);
INSERT INTO `yahoo_condition` VALUES(26,'cloudy',0,0,0);
INSERT INTO `yahoo_condition` VALUES(27,'mostly cloudy (night)',0,0,0);
INSERT INTO `yahoo_condition` VALUES(28,'mostly cloudy (day)',0,0,0);
INSERT INTO `yahoo_condition` VALUES(29,'partly cloudy (night)',0,0,0);
INSERT INTO `yahoo_condition` VALUES(30,'partly cloudy (day)',0,0,0);
INSERT INTO `yahoo_condition` VALUES(31,'clear (night)',0,0,0);
INSERT INTO `yahoo_condition` VALUES(32,'sunny',0,0,0);
INSERT INTO `yahoo_condition` VALUES(33,'fair (night)',0,0,0);
INSERT INTO `yahoo_condition` VALUES(34,'fair (day)',0,0,0);
INSERT INTO `yahoo_condition` VALUES(35,'mixed rain and hail',1,0,0);
INSERT INTO `yahoo_condition` VALUES(36,'hot',0,0,0);
INSERT INTO `yahoo_condition` VALUES(37,'isolated thunderstorms',0,0,0);
INSERT INTO `yahoo_condition` VALUES(38,'scattered thunderstorms',0,0,0);
INSERT INTO `yahoo_condition` VALUES(39,'scattered thunderstorms',0,0,0);
INSERT INTO `yahoo_condition` VALUES(40,'scattered showers',1,0,0);
INSERT INTO `yahoo_condition` VALUES(41,'heavy snow',0,1,0);
INSERT INTO `yahoo_condition` VALUES(42,'scattered snow showers',0,1,0);
INSERT INTO `yahoo_condition` VALUES(43,'heavy snow',0,1,0);
INSERT INTO `yahoo_condition` VALUES(44,'partly cloudy',0,0,0);
INSERT INTO `yahoo_condition` VALUES(45,'thundershowers',1,0,0);
INSERT INTO `yahoo_condition` VALUES(46,'snow showers',1,1,0);
INSERT INTO `yahoo_condition` VALUES(47,'isolated thundershowers',1,0,0);
INSERT INTO `yahoo_condition` VALUES(3200,'not available',0,0,1);

COMMIT;
